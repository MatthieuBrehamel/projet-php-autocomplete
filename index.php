
<?php //PHP | Récupération de la liste des styles depuis un fichier json
$fichier = "./styles_musicaux.json";
$donnees = file_get_contents($fichier);
$objet = json_decode($donnees,true);
$existe = false;
$tab=array();

if(isset($_GET['term'])){
    foreach($objet['styles'] as $style){
        if(preg_match('/^('.$_GET['term'].')/i', $style)!=NULL){
            array_push($tab,$style);
        }
    }
}
if(isset($_GET['group'])){
    foreach($objet['groupes'] as $groupe){
        if(strcasecmp($_GET['group'], $groupe) == 0){
            $existe = true;
    
        }
    }
    $tab['existe'] = $existe;
}
echo json_encode($tab);

?>
